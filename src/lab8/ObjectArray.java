package lab8;

public class ObjectArray {
	
	private static final int MAX = 100;
	
	private Object [] array;
	private int index;
	private int pointer;

	public ObjectArray() {
		this.array = new Object [MAX];
		this.index = 0;
		this.pointer = 0;
	}
	
	public ObjectArray(int size) {
		this.array = new Object [size];
		this.index = 0;
		this.pointer = 0;
	}
	
	public ObjectArray(Object [] anArr, int newIndex) {
		this.array = anArr;
		this.index = newIndex;
		this.pointer = 0;
	}
	
	public void reset() {
		this.pointer = 0;
	}
	
	public boolean hasNext() {
		if (this.pointer < this.index) 
			return true;
		else 
			return false;
	}
	
	public Object getNext() {
		return this.array[this.pointer++];
	}
	
	public Object[] getArray() {
		Object [] copy = this.array;
		return copy;
	}
	
	public void setArray(Object [] anArr) {
		this.array = anArr;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public void setIndex(int anIndex) throws IndexOutOfBoundsException {
		if (anIndex < 0 || anIndex >= this.array.length)
			throw new IndexOutOfBoundsException();
		else this.index = anIndex;
	}
	
	public Object getObject(int pos) throws IndexOutOfBoundsException {
		if (pos < 0 || pos >= this.array.length)
			throw new IndexOutOfBoundsException();
		else {
			Object copy = this.array[pos];
			return copy;
		}
	}
	
	public String toString() {
		String toString = "";
		for (int i = 0; i < this.index; i++) {
			toString = toString + this.array[i].toString() + "\n";
		}
		return toString;
	}
	
	public boolean equals(ObjectArray anArr) {
		if (this.index == anArr.getIndex()
				&& this.array.length == anArr.getArray().length) {
			Object [] otherArray = anArr.getArray();
			for (int i = 0; i < this.index; i ++) {
				if (!this.array[i].equals(otherArray[i]))
					return false;
			}
			return true;
		}
		
		else return false;
	}
	
	public void add(Object obj) {
		if (this.isFull()) 
			return;
		else {
			this.array[this.index] = obj;
			index++;
		}
	}
	
	public void delete(int pos) throws IndexOutOfBoundsException {
		if (pos < 0 || pos > this.array.length)
			throw new IndexOutOfBoundsException();
		else if (this.isEmpty() || pos >= this.index)
			return;
		else {
			Object temp1 = this.array[this.index - 1];
			Object temp2 = null;
			for (int i = this.index - 2; i >= pos; i--) {
				temp2 = this.array[i];
				this.array[i] = temp1;
				temp1 = temp2;
			}
			index--;
		}
	}
	
	public void delete(Object anObj) {
		if (this.isEmpty())
			return;
		else {
			for (int i = 0; i < this.index; i++) {
				if (this.array[i].equals(anObj)) this.delete(i);
			}
		}
	}
	
	public void insert(int pos, Object anObj) throws IndexOutOfBoundsException {
		if (pos < 0 || pos > this.array.length)
			throw new IndexOutOfBoundsException();
		else if (this.isFull() || pos > this.index) 
			return;
		else if (pos == this.index) {
			this.add(anObj);
			return;
		} else {
			Object temp1 = this.array[pos];
			Object temp2 = null;
			this.array[pos] = anObj;
			for (int i = pos + 1; i <= this.index; i++) {
				temp2 = this.array[i];
				this.array[i] = temp1;
				temp1 = temp2;
			}
		}
		index++;
	}
	
	public int isThere(Object anObj) {
		if (this.index == 0) return -1;
		else {
			for (int i = 0; i < this.index; i++) {
				if (this.array[i].equals(anObj)) return i;
			}
		}
		return -1;
	}
	
	public boolean isFull() {
		if (this.index >= this.array.length) return true;
		else return false;
	}
	
	public boolean isEmpty() {
		if (this.array.length == 0) return false;
		else if (this.index == 0) return true;
		else return false;
	}
	
	void clear() {
		this.setIndex(0);
	}
	
	void trim() {
		Object [] trimmed = new Object [this.index];
		for (int i = 0; i < this.index; i++) {
			trimmed[i] = this.array[i];
		}
		this.setArray(trimmed);
	}
	
	void moreCapacity(int factor) {
		if (factor < 1) return;
		Object [] enlarged = new Object [this.array.length * factor];
		for (int i = 0; i < this.index; i++) {
			enlarged[i] = this.array[i];
		}
		this.setArray(enlarged);
	}
	
	void moreCapacity(double factor) {
		if (factor < 1.0) return;
		Object [] enlarged = new Object [(int)(this.array.length * factor)];
		for (int i = 0; i < this.index; i++) {
			enlarged[i] = this.array[i];
		}
		this.setArray(enlarged);
	}
}
