package lab8;

public class Problem1_App {

	public static void main(String[] args) {
		ReadFile reader = new ReadFile("elStops.txt");
		ObjectArray elstops = new ObjectArray();
		reader.read(elstops);
		int num = 0;
		while(elstops.hasNext()) {
			System.out.println(((ELStop) elstops.getNext()).getGPS().getName());
			num++;
		}
		System.out.println("Number of objects got: " + num + 
				"\nIndex: " + elstops.getIndex());
		
		//pointer should be == index
		
		reader.close();

	}

}
