package lab8;

public class Rectangle {
	
	private static int num_rectangles = 0;
	public static int numRectangles() {
		return num_rectangles;
	}
	
	private double width;
	private double length;
	
	public Rectangle() {
		this.width = 3.0;
		this.length = 4.0;
		num_rectangles++;
	}
	
	public Rectangle(double width_in, double length_in) {
		this.width = width_in;
		this.length = length_in;
		num_rectangles++;
	}
	
	public void setWidth(double width_in) {
		this.width = width_in;
	}
	
	public void setLength(double length_in) {
		this.length = length_in;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getLength() {
		return this.length;
	}
	
	public int compareTo(Rectangle rect_in) {
		if(this.length > rect_in.getLength())
			return 1;
		else if (this.length == rect_in.getLength())
			return 0;
		else 
			return -1;
	}
	
	public boolean equals(Rectangle rect_in) {
		if(this.length == rect_in.getLength()
				&& this.width == rect_in.getWidth())
			return true;
		else return false;
	}
	
	public String toString() {
		return this.width + " X " + this.length;
	}
	
	public double calcArea() {
		return this.width * this.length;
	}
	
}
