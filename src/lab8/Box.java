package lab8;

public class Box extends Rectangle {
	
	private static int num_boxes = 0;
	public static int numBoxes() {
		return num_boxes;
	}
	
	private double height;
	
	public Box() {
		super();
		this.height = 5.0;
		num_boxes++;
	}
	
	public Box(double height_in) {
		super();
		this.height = height_in;
		num_boxes++;
	}
	
	
	public Box(double width_in, double length_in, double height_in) {
		super(width_in, length_in);
		this.height = height_in;
		num_boxes++;
	}
	
	public void setHeight(double height_in) {
		this.height = height_in;
	}
	
	public double getHeight() {
		return this.height;
	}
	
	public String toString() {
		return super.toString() + " X " + this.height;
	}
	
	public boolean equals(Box box_in) {
		if(this.getWidth() == box_in.getWidth() &&
				this.getLength() == box_in.getLength() &&
				this.getHeight() == box_in.getHeight())
			return true;
		else return false;
	}
	
	public double calcVolume() {
		return super.calcArea() * this.height;
	}
	

}
