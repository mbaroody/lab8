package lab8;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class ReadFile {
	private Scanner fileReader;
	
	public ReadFile(String path) {
		FileReader aFile = null;
		try {
			aFile = new FileReader(path);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fileReader = new Scanner(aFile);
	}
	
	public void read(ObjectArray anArr) {
		String [] splitted;
		boolean isUnderConstruction = false;
		while(this.fileReader.hasNext()) {
			if(anArr.isFull())
				anArr.moreCapacity(1.5);
			splitted = this.fileReader.nextLine().split(",");
			if (splitted.length == 9)
				isUnderConstruction = true;
			if (isUnderConstruction) {
				anArr.add(new StopUnderConstruction(Integer.parseInt(splitted[0]),
						(splitted[1].isEmpty() ? -1 : Integer.parseInt(splitted[1])), 
						new GPSLocation(splitted[2], 
							Double.parseDouble(splitted[3]),
							Double.parseDouble(splitted[4])), 
						(splitted[5].isEmpty() ? -1 : Integer.parseInt(splitted[5])), 
						(splitted[6].isEmpty() ? -1 : Integer.parseInt(splitted[6])), 
						(splitted[7] == "0" ? false : true),
						(Integer.parseInt(splitted[8]))));
			} else {
				anArr.add(new ELStop(Integer.parseInt(splitted[0]),
						(splitted[1].isEmpty() ? -1 : Integer.parseInt(splitted[1])), 
						new GPSLocation(splitted[2], 
							Double.parseDouble(splitted[3]),
							Double.parseDouble(splitted[4])), 
						(splitted[5].isEmpty() ? -1 : Integer.parseInt(splitted[5])), 
						(splitted[6].isEmpty() ? -1 : Integer.parseInt(splitted[6])), 
						(splitted[7] == "0" ? false : true)));
			}
			isUnderConstruction = false;
		}
	}
	
	public void close() {
		fileReader.close();
	}
}
