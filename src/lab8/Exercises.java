package lab8;

public class Exercises {

	public static void main(String[] args) {
		
		//****************************//
		//exercise 1
		//*****************************//
		Rectangle rectangle = new Rectangle(3.0, 4.0);
		Box box = new Box(3.0, 4.0, 5.0);
		
		System.out.println("Instantiated a " + rectangle.toString() + 
				" rectangle.\n"
				+ "Instantiated a " + box.toString() + " box.");
		
		//****************************//
		//exercise 2
		//****************************//
		System.out.println("Width rectangle: " + rectangle.getWidth());
		System.out.println("Length rectangle: " + rectangle.getLength());
		System.out.println("Width box: " + box.getWidth());
		System.out.println("Length box: " + box.getLength());
		System.out.println("Height box: " + box.getHeight());
		System.out.println("Changing all values to 10...");
		rectangle.setWidth
		(10.0); rectangle.setLength(10.0);
		box.setHeight(10.0); box.setLength(10.0); box.setWidth(10.0);
		System.out.println("Width rectangle: " + rectangle.getWidth());
		System.out.println("Length rectangle: " + rectangle.getLength());
		System.out.println("Width box: " + box.getWidth());
		System.out.println("Length box: " + box.getLength());
		System.out.println("Height box: " + box.getHeight());
		
		//****************************//
		//exercise 3
		//**************************//
		//Defined in box class, 
		//I over rode equals() and toString()
		//calcVolume is the only new method
		
		//****************************//
		//exercise 4 
		//*****************************//
		Rectangle rect1 = new Box(5.0);
		
		//won't work, rectangle is not a box
		//System.out.println("Volume: " + rect1.calcVolume());
		//but casting will
		
		System.out.println("Volume: " + ((Box) (rect1)).calcVolume());
		
		//interesting that it calls the box one, but not for calcVolume
		//i guess it can only call overriden methods, not new methods in the
		//subclass
		
		System.out.println("A " + rect1.toString() + " thing.");
		System.out.println("Area: " + rect1.calcArea());
		
		//****************************//
		//exercise 5
		//****************************//
		//submitted as pdf 
		
		//****************************//
		//exercise 6
		//****************************//
		
		Rectangle [] rectArray = new Rectangle [10];
		for (int i = 0; i < rectArray.length; i++) {
			rectArray[i] = new Rectangle((double)i + 1, (double)i + 1);
		}
		rectArray[0] = new Box(1.0, 2.0, 3.0);
		rectArray[1] = new Box(2.0, 3.0, 4.0);
		for (int j = 0; j < rectArray.length; j++) {
			System.out.println("Spot " + j + ": " + rectArray[j].toString());
		}
		//answer: you can store boxes in rectangle arrays
		//because all boxes are rectangles
		
		Box [] boxArray = new Box [10];
		for (int a = 0; a < boxArray.length; a++) {
			boxArray[a] = new Box((double)a+1, (double)a+1, (double)a+1);
		}
		
		
		//this won't work: not all rectangles are boxes
		//boxArray[0] = new Rectangle(1.0, 2.0);
		//boxArray[1] = new Rectangle(2.0, 3.0);
		
		for (int b = 0; b < boxArray.length; b++) {
			System.out.println("Spot " + b + ": " + boxArray[b].toString());
		}
		
		
	}

}
