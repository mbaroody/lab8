package lab8;

public class Problem2_App {

	public static void main(String[] args) {
		ReadFile reader = new ReadFile(args[0]);
		ObjectArray stops = new ObjectArray();
		reader.read(stops);
		int numSuper = 0;
		int numSub = 0;
		ELStop temp;
		while (stops.hasNext()) {
			temp = (ELStop) stops.getNext();
			if (temp instanceof StopUnderConstruction) {
				System.out.println(temp.getGPS().getName() + ", construction status: "
						+ StopUnderConstruction.LEVEL_NAMES[((StopUnderConstruction) temp).getLevel()]);
				numSub++;
			} else {
				System.out.println(temp.getGPS().getName());
				numSuper++;
			}
		}
		
		System.out.println("\nNum stops under construction: " + numSub +
				"\nNum fully fuctional stops: " + numSuper);
		
		System.out.println("\nNum objects returned by getNext(): " + (numSuper + numSub) +
				"\nIndex: " + stops.getIndex());
		
		reader.close();

	}

}
